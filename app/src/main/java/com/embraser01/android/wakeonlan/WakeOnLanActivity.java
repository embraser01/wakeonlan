/*
Copyright (C) 2014 Marc-Antoine Fernandes
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
* Neither the name of the author nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.embraser01.android.wakeonlan;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.support.v7.widget.Toolbar;

import java.util.List;

/**
 *  Main Activity, to manage fragments
 */

public class WakeOnLanActivity extends ActionBarActivity {
	
	// Initialisation des variables
	
	private ListDisplay mListDisplay;
	private NewPCDisplay mNewPCDisplay;
	private ListPC mListPC;
    private Toolbar toolbar;

	public FragmentManager fm;
	public FragmentTransaction ft;
	public DialogFragment newFragment;

    protected int ordermode = 0;


	// Methodes appelees au lancementde l'activite

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_fragment);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


		setupFragments();
		showFragment(this.mListDisplay, ListDisplay.TAG);
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        menu.findItem(R.id.menu_created).setChecked(true);


        //final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        //SearchView mSearchBox = (SearchView) menu.findItem(R.id.menu_search).getActionView();
       //mSearchBox.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        return true;
    }

    // Methode appelee quand un bouton de l'actionBar est actionnee

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.menu_created:
                ordermode = 0;
                updateList();
                item.setChecked(true);
                break;
            case R.id.menu_lastused:
                ordermode = 1;
                updateList();
                item.setChecked(true);
                break;
            case R.id.menu_usedcount:
                ordermode = 2;
                updateList();
                item.setChecked(true);
                break;
            case R.id.menu_alpha:
                ordermode = 3;
                updateList();
                item.setChecked(true);
                break;
        }
        return true;
    }


    // Setup of the ToolBar

    /*public void setupToolbar(){


        toolbar.getMenu().clear();
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        toolbar.setOnMenuItemClickListener(

                new Toolbar.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_created:
                                ordermode = 0;
                                updateList();
                                item.setChecked(true);
                                break;
                            case R.id.menu_lastused:
                                ordermode = 1;
                                updateList();
                                item.setChecked(true);
                                break;
                            case R.id.menu_usedcount:
                                ordermode = 2;
                                updateList();
                                item.setChecked(true);
                                break;
                            case R.id.menu_alpha:
                                ordermode = 3;
                                updateList();
                                item.setChecked(true);
                                break;
                        }
                        return true;
                    }
                });

        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.getMenu().findItem(R.id.menu_created).setChecked(true);
        toolbar.setTitle(R.string.title_wake);

    }*/


	// Initialisation des differents Fragments
	
	private void setupFragments() {
		fm = getFragmentManager();

        if ( fm.findFragmentByTag(ListDisplay.TAG) != null)
            fm.beginTransaction().remove(this.mListDisplay);
		this.mListDisplay = new ListDisplay();
				
		
		this.mNewPCDisplay = (NewPCDisplay) fm.findFragmentByTag(NewPCDisplay.TAG);

		if (this.mNewPCDisplay == null) {
			this.mNewPCDisplay = new NewPCDisplay();
		}
	}
	
	// Methode pour afficher le fragments voulu
	
	private void showFragment(final Fragment fragment, String tag) {
		if (fragment == null)
			return;

		ft = fm.beginTransaction();
		
		ft.replace(R.id.FrameLayout, fragment, tag);
		
		ft.commit();
	}
	
	
	// Methode pour afficher un DialogFragments
	
	public void showDialogFragment(DialogFragment dialogFragment, boolean edit_mode, int int_id, boolean duplicate_mode) {
		
		FragmentTransaction ft = fm.beginTransaction();
		Fragment prev = fm.findFragmentByTag(NewPCDisplay.TAG);
		
		if ( prev != null)
			ft.remove(prev);
		ft.addToBackStack(null);

        // On test si on est en mode clone si oui alors affichage avec edit et nouvel id
        if (duplicate_mode == true)
		    newFragment = new NewPCDisplay(int_id, true);
        else
            newFragment = new NewPCDisplay(edit_mode, int_id);
		
		newFragment.show(getFragmentManager(), NewPCDisplay.TAG);
		ft.commit();
	}


	// Methode pour actualisation de la ListView

	public void updateList() {
		setupFragments();
		showFragment(mListDisplay,ListDisplay.TAG);
		
	}

    public void addPC (){
        showDialogFragment(this.mNewPCDisplay, false, 0, false);
    }

	public void updatePC ( int id) {
		showDialogFragment(this.mNewPCDisplay, true, id, false);
	}

    public void duplicatePC (int id) {
        showDialogFragment(this.mNewPCDisplay, true, id, true);
    }
	
	public void deletePC(List<Integer> ids) {
		mListPC = new ListPC(this);
		
		for(int i=0 ; i < ids.size() ; i++) {
            mListPC.deletePC(ids.get(i));
            Log.e("JEEJ", Integer.toString(ids.get(i)));
        }
	}


    public Toolbar getToolbar(){
        return toolbar;
    }

    public FragmentManager getActivityFragmentManager() {
        return getFragmentManager();
    }
}