package com.embraser01.android.wakeonlan;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc-Antoine on 29/12/2014.
 */

public class ListPCAdapter extends RecyclerView.Adapter<ListPCAdapter.ViewHolder>{

    private List<NewPC> pclist;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public ListPCAdapter(List<NewPC> pclist) {
        this.pclist = pclist;
    }


    public void toggleSelection(int pos) {
        if (selectedItems.get(pos,false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }

        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {

        List<Integer> items =
        new ArrayList<Integer>(selectedItems.size());

        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(pclist.get(selectedItems.keyAt(i)).getId());
        }
        return items;
    }



    @Override
    public int getItemCount() {
        return pclist.size();
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        NewPC pc = pclist.get(i);
        viewHolder.vTitle.setText(pc.getName());
        viewHolder.vMac.setText(pc.getMac());
        viewHolder.vIp.setText(pc.getIP());
        viewHolder.vPort.setText(pc.getPort());
        viewHolder.itemView.setActivated(selectedItems.get(i,false));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.state_selector, viewGroup, false);

        return new ViewHolder(itemView);
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView vTitle;
        protected TextView vMac;
        protected TextView vIp;
        protected TextView vPort;

        public ViewHolder(View v) {
            super(v);
            vTitle =  (TextView) v.findViewById(R.id.list_row_title);
            vMac = (TextView)  v.findViewById(R.id.list_row_mac);
            vIp = (TextView)  v.findViewById(R.id.list_row_ip);
            vPort = (TextView) v.findViewById(R.id.list_row_port);
        }
    }
}
