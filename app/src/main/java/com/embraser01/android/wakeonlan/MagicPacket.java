/*
Copyright (C) 2008-2014 Matt Black and
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
* Neither the name of the author nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.embraser01.android.wakeonlan;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *	Static WOL magic packet class
 */
public class MagicPacket
{
	private static final String TAG = "MagicPacket";

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Return clean and verified mac address
     * @param mac
     * @return
     * @throws IllegalArgumentException
     */

	public static String validateMac(String mac) throws IllegalArgumentException
	{
		// regexp pattern match a valid MAC address
		final Pattern pat = Pattern.compile("((([0-9a-fA-F]){2}[:]){5}([0-9a-fA-F]){2})");
		final Matcher m = pat.matcher(mac);

		if(m.find()) {
			String result = m.group();
			return result;
		}else{
			throw new IllegalArgumentException("Invalid MAC address");
		}
	}

    /**
     * Send MagicPacket
     * @param mac
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     */
	public static String send(String mac, String ip, int port) throws IOException, IllegalArgumentException
	{
        String mac_list ="";
        for(int i=0,j =0;i<17;i++){ // convert mac to list without ":"
            if(mac.charAt(i) != ':' ) {
                mac_list+= mac.charAt(i) + "" + mac.charAt(i+1) ;
                j++;
                i++;
            }
        }
        mac_list= mac_list.toUpperCase();

        byte[] macBytes = hexStringToByteArray(mac_list);

        final byte[] trame = new byte[102];

		// fill first 6 bytes of FF
		for(int i=0; i<6; i++) {
			trame[i] = (byte) 0xff;
		}

		// fill remaining bytes with target MAC
		for(int i=6; i<trame.length; i+=macBytes.length) {
			System.arraycopy(macBytes, 0, trame, i, macBytes.length);
		}

		// create socket to IP
        try {
            new NetTask(ip,trame,port).execute().get();
        } catch (ExecutionException e) {
            return e.getMessage();
        } catch (InterruptedException e) {
            return e.getMessage();
        }
		return bytesToHex(trame);
	}

    /**
     * Return mac in byte[]
     * @param s
     * @return
     */

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /**
     * Return String of a byte[]
     * @param bytes
     * @return
     */

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
