package com.embraser01.android.wakeonlan;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.melnykov.fab.FloatingActionButton;

import java.io.IOException;
import java.util.List;

/**
 * class permettant d'afficher la liste et le menu contextuel
 */

public class ListDisplay extends Fragment implements RecyclerView.OnItemTouchListener, View.OnClickListener, ActionMode.Callback{
	
	public static String TAG = "ListDisplay";

    protected int ordermode;

    private boolean ActionModeState = false;


	ListPC listBDD;
    RecyclerView recList;
    ListPCAdapter listPCAdapter;

	MenuItem editBtn;
    MenuItem cloneBtn;

    FloatingActionButton fab;
    GestureDetectorCompat gestureDetector;
    ActionMode actionMode;


	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.history_list,container , false);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);


        fab = (FloatingActionButton) getView().findViewById(R.id.fab);


        listBDD = new ListPC(getActivity());
        listBDD.open();
        ordermode = ((WakeOnLanActivity) getActivity()).ordermode;


        if (listBDD.getCursor(ordermode).getCount() == 0) {
            TextView ifempty = (TextView) getView().findViewById(R.id.empty);
            ifempty.setText(getString(R.string.text_empty));
        }
        else {

            recList = (RecyclerView) getView().findViewById(R.id.list);
            recList.setHasFixedSize(true);
            recList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);
            recList.setItemAnimator(new DefaultItemAnimator());

            listPCAdapter = new ListPCAdapter(listBDD.selectAll(ordermode));
            recList.setAdapter(listPCAdapter);


            recList.addOnItemTouchListener(this);

            gestureDetector = new GestureDetectorCompat(getActivity(), new RecyclerViewDemoOnGestureListener());

            // FAB
            fab.attachToRecyclerView(recList);
            fab.show();
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((WakeOnLanActivity) getActivity()).addPC();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {

        if (view == null) {
            return;
        }
        else {
            // item click
            int idx = recList.getChildPosition(view);

            /*
             * Mode selection
             */
            if (ActionModeState) {
                listPCAdapter.toggleSelection(idx);
                if ( listPCAdapter.getSelectedItemCount() == 0)
                    actionMode.finish();

                String title = listPCAdapter.getSelectedItemCount() + " " + getString(R.string.selected_count);
                actionMode.setTitle(title);
                return;
            }
            else {

            /*
             * Mode pas selection :D
             */

                // TODO Cardview
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Creation du menu
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.contextual_menu, menu);

        //((WakeOnLanActivity)getActivity()).getSupportActionBar().hide();
        fab.setVisibility(View.GONE);

        editBtn = (MenuItem) menu.findItem(R.id.context_menu_edit);
        cloneBtn = (MenuItem) menu.findItem(R.id.context_menu_duplicate);

        ActionModeState = true;
        return true;
    }

    @Override
    public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
        List<Integer> checkedList = listPCAdapter.getSelectedItems();
        switch (item.getItemId()){
            case R.id.context_menu_launch:
                sendPacket(checkedList);
                break;

            case R.id.context_menu_edit:
                ((WakeOnLanActivity) getActivity()).updatePC(checkedList.get(0));
                break;

            case R.id.context_menu_delete:
                deleteSelectedPC(checkedList);
                break;

            case R.id.context_menu_duplicate:
                ((WakeOnLanActivity) getActivity()).duplicatePC(checkedList.get(0));
                break;

        }
        mode.finish();
        return false;

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        listPCAdapter.clearSelections();
        fab.setVisibility(View.VISIBLE);
        ActionModeState = false;
    }


    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = recList.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = recList.findChildViewUnder(e.getX(), e.getY());
            if (ActionModeState || view == null) {
                return;
            }
            // Start the CAB using the ActionMode.Callback defined above
            actionMode = ((WakeOnLanActivity)getActivity()).startSupportActionMode(ListDisplay.this);
            int idx = recList.getChildPosition(view);
            listPCAdapter.toggleSelection(idx);
            String title = "1 " + getString( R.string.selected_count);
            actionMode.setTitle(title);
            super.onLongPress(e);
        }
    }



    public void sendPacket(List<Integer> sendList) {
        for(int i = 0; i< sendList.size();i++){
            String msgReturn = "";

            String pcDetail[] = listBDD.selectPC((int) sendList.get(i));
            try {
                msgReturn = MagicPacket.send(pcDetail[1],pcDetail[2],Integer.parseInt(pcDetail[3]));
            } catch (IOException e) {
                msgReturn = e.getMessage();
                e.printStackTrace();
            }
            Toast.makeText(getActivity(),getResources().getString(R.string.packet_sent) + pcDetail[0],Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteSelectedPC(final List<Integer> deleteList){
        new MaterialDialog.Builder(getActivity())
                .title(R.string.alert_delete_title)
                .content(R.string.alert_delete_msg)
                .positiveText(R.string.alert_delete_ybtn)
                .negativeText(R.string.alert_delete_nbtn)
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        ((WakeOnLanActivity) getActivity()).deletePC(deleteList);
                        ((WakeOnLanActivity) getActivity()).updateList();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
        }).show();
    }
}