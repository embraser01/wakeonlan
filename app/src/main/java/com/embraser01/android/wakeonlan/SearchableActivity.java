package com.embraser01.android.wakeonlan;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;


/**
 * Created by Marc-Antoine on 24/09/2014.
 */
public class SearchableActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            String query = getIntent().getStringExtra(SearchManager.QUERY);
            Log.e("lawl",query);
            doMySearch(query);

        }
    }

    public void doMySearch(String query) {
        ListPC listPC = new ListPC(getApplicationContext());
        listPC.open();
        Cursor searchCursor = listPC.getWordMatches(query);
        listPC.close();
        ListDisplay listDisplay = (ListDisplay) ((WakeOnLanActivity)getBaseContext()).getActivityFragmentManager().findFragmentByTag(ListDisplay.TAG);
        if (listDisplay == null)
            Log.e("lawl","cest tro nul");
        if (searchCursor == null)
            Log.e("lawl","c'est encore plus nul");
       // listDisplay.searchAdapter(searchCursor);

    }
}
