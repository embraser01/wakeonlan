package com.embraser01.android.wakeonlan;

/**
 * class qui sert pas a grand chose xD a part a rassembler les infos dans une seule variable
 */

public class NewPC {
	
	private int id;
	private String name;
	private String mac;
	private String ip;
	private String port;
	
	public NewPC() {}
 
	public NewPC(String name, String mac, String ip, String port ){
		this.name = name;
		this.mac = mac;
		this.ip = ip;
		this.port = port;
	}

    public NewPC(String name, String mac, String ip, String port, int id){
        this.name = name;
        this.mac = mac;
        this.ip = ip;
        this.port = port;
        this.id = id;
    }
 
	public int getId() {
		return id;
	}
 
	public void setId(int id) {
		this.id = id;
	}
 
	
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
	
 
	public String getMac() {
		return mac;
	}
 
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	
	public String getIP() {
		return ip;
	}
	
	public void setIP(String ip) {
		this.ip = ip;
	}
	
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	 
	public String toString(){
		return R.string.title + " : " + name + 
				"\n" + R.string.mac + " : " + mac +
				"\n" + R.string.ip + " : " + ip +
				"\n" + R.string.port + " : " + port;
	}
}
