package com.embraser01.android.wakeonlan;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by Marc-Antoine on 09/10/2014.
 */
public class NetTask extends AsyncTask<String, Integer, InetAddress>
{
    private final String ip;
    private final byte[] trame;
    private final int port;

    public NetTask(String ip, byte[] trame, int port) {
        this.ip = ip;
        this.trame = trame;
        this.port = port;
    }

    @Override
    protected InetAddress doInBackground(String... params)
    {
        InetAddress addr = null;
        try
        {
            addr = InetAddress.getByName(ip);
            final DatagramPacket packet = new DatagramPacket(trame,0,trame.length,addr,port);
            final DatagramSocket socket = new DatagramSocket();
            socket.send(packet);
            socket.close();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
        return addr;
    }

}

