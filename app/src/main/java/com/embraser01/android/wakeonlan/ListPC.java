package com.embraser01.android.wakeonlan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;

import java.util.ArrayList;
import java.util.List;

/**
 * class qui gère la BDD lecture/ecriture
 */
public class ListPC {
	

	protected static final String TABLE_PC = BaseSQL.TABLE_PC;
	
	protected static final String COL_ID = BaseSQL.COL_ID;
	protected static final String COL_NAME = BaseSQL.COL_NAME;
	protected static final String COL_MAC = BaseSQL.COL_MAC;
	protected static final String COL_IP = BaseSQL.COL_IP;
	protected static final String COL_PORT = BaseSQL.COL_PORT;
    protected static final String COL_LASTUSED = BaseSQL.COL_LASTUSED;
    protected static final String COL_USED_CNT = BaseSQL.COL_USED_CNT;


    protected static final int NUM_COL_ID = BaseSQL.NUM_COL_ID;
    protected static final int NUM_COL_NAME = BaseSQL.NUM_COL_NAME;
    protected static final int NUM_COL_MAC = BaseSQL.NUM_COL_MAC;
    protected static final int NUM_COL_IP = BaseSQL.NUM_COL_IP;
    protected static final int NUM_COL_PORT = BaseSQL.NUM_COL_PORT;
    protected static final int NUM_COL_LASTUSED = BaseSQL.NUM_COL_LASTUSED;
    protected static final int NUM_COL_USED_CNT = BaseSQL.NUM_COL_USED_CNT;
	
	private static SQLiteDatabase bdd;
	
	private BaseSQL myBaseSQL;

	
	public ListPC(Context context) {
		// Creation - Chargement de la bdd + table
		myBaseSQL = new BaseSQL(context, "listpc.db", null);
	}

	public void open() {
		//ouverture de la BDD pour l'ecriture
		bdd = myBaseSQL.getWritableDatabase();
	}
	
	public void close() {
		//fermeture de la BDD pour l'ecriture
		bdd.close();
	}
	
	public SQLiteDatabase getBDD() {
		return bdd;
	}


    // Action sur la BDD

	public Cursor getCursor(int ordermode) {
        String orderby;
        switch (ordermode){
            case 1:
                // Dernier Utilise
                orderby = " ORDER BY " + COL_LASTUSED + " DESC, " + COL_ID + " DESC";
                break;
            case 2:
                // Le plus utilise
                orderby = " ORDER BY " + COL_USED_CNT + " DESC, " + COL_ID + " DESC";
                break;
            case 3:
                // Par ordre alphabetic
                orderby = " ORDER BY " + COL_NAME + ", " + COL_ID + " DESC";
                break;
            default:
                // Le dernier cree
                orderby = " ORDER BY " + COL_ID + " DESC";
                break;
        }

		Cursor c = bdd.rawQuery("SELECT *" +
                " FROM " + TABLE_PC +
                orderby , null);
		return c;
	}

    public Cursor getWordMatches(String search_words) {
        Cursor c = bdd.rawQuery("SELECT *" +
                " FROM " + TABLE_PC +
                " WHERE " + COL_NAME + " = LOWER(?)" +
                " OR LOWER(" + COL_IP + ") = LOWER(?)" +
                " OR LOWER(" + COL_MAC + ") = LOWER(?)", new String[]{search_words});

        if (c == null)
            return null;
        else
            return c;
    }
	
	public long newPC(NewPC pc) {
		ContentValues values = new ContentValues();
		values.put(COL_NAME, pc.getName());
		values.put(COL_MAC, pc.getMac());
		values.put(COL_IP, pc.getIP());
		values.put(COL_PORT, pc.getPort());

        String datetime = (DateFormat.format("yyyy-MM-dd hh:mm", new java.util.Date()).toString());
        values.put(COL_LASTUSED, datetime );

        values.put(COL_USED_CNT, 0);
		
		return bdd.insert(TABLE_PC, null, values);
	}
	
	public int updatePC(int id, NewPC pc) {
		ContentValues values = new ContentValues();
		values.put(COL_NAME, pc.getName());
		values.put(COL_MAC, pc.getMac());
		values.put(COL_IP, pc.getIP());
		values.put(COL_PORT, pc.getPort());

        String datetime = (DateFormat.format("yyyy-MM-dd hh:mm", new java.util.Date()).toString());
        values.put(COL_LASTUSED, datetime );
		
		return bdd.update(TABLE_PC,values, COL_ID + " = " + id,null);
	}


	public int deletePC(int id) {

		return bdd.delete(TABLE_PC, COL_ID + " = " + id,  null);
	}

    public List<NewPC> selectAll(int ordermode){

        Cursor c = this.getCursor(ordermode);
        c.moveToFirst();

        List<NewPC> newPCList = new ArrayList<NewPC>();

        for(;!c.isLast();c.moveToNext()) {

            NewPC myPC = new NewPC(c.getString(NUM_COL_NAME),
                    c.getString(NUM_COL_MAC),
                    c.getString(NUM_COL_IP),
                    c.getString(NUM_COL_PORT),
                    c.getInt(NUM_COL_ID));

            newPCList.add(myPC);
        }

        if ( c.getCount() != 0){
            NewPC myPC = new NewPC(c.getString(NUM_COL_NAME),
                    c.getString(NUM_COL_MAC),
                    c.getString(NUM_COL_IP),
                    c.getString(NUM_COL_PORT),
                    c.getInt(NUM_COL_ID));

            newPCList.add(myPC);
        }

        return newPCList;
    }

	public  String[] selectPC(int id) {
		Cursor c = this.getCursor(0);
		c.moveToFirst();
		for(;id != c.getInt(NUM_COL_ID);c.moveToNext()){}
		String myPC [] = { c.getString(NUM_COL_NAME),
						c.getString(NUM_COL_MAC),
						c.getString(NUM_COL_IP),
						c.getString(NUM_COL_PORT) };
		return myPC;
	}
}
