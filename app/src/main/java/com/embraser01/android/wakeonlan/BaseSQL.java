package com.embraser01.android.wakeonlan;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * class qui permet de creer une BDD ou juste la charger
 */

public class BaseSQL extends SQLiteOpenHelper {

    private static final int VERSION_BDD = 7;
	
	protected static final String TABLE_PC = "table_pc";
	protected static final String COL_ID = "_id";
	protected static final String COL_NAME = "Name";
	protected static final String COL_MAC = "Adresse_mac";
	protected static final String COL_IP = "Adresse_IP";
	protected static final String COL_PORT = "Port";
    protected static final String COL_LASTUSED = "Last_Used";
    protected static final String COL_USED_CNT = "Used_cnt";

    protected static final int NUM_COL_ID = 0;
    protected static final int NUM_COL_NAME = 1;
    protected static final int NUM_COL_MAC = 2;
    protected static final int NUM_COL_IP = 3;
    protected static final int NUM_COL_PORT = 4;
    protected static final int NUM_COL_LASTUSED = 5;
    protected static final int NUM_COL_USED_CNT = 6;
 
	private static final String CREATE_BDD = "CREATE TABLE " + TABLE_PC + " ("
	+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME + " TEXT NOT NULL, "
	+ COL_MAC + " TEXT NOT NULL, " + COL_IP + " TEXT NOT NULL, " + COL_PORT + " TEXT NOT NULL, "
    + COL_LASTUSED + " TEXT, " + COL_USED_CNT + " INTEGER);";

	
	public BaseSQL(Context context, String name, CursorFactory factory) {
		super(context, name, factory, VERSION_BDD);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// on cree la table à partir de la requete écrite dans la variable CREATE_BDD
		db.execSQL(CREATE_BDD);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE " + TABLE_PC + ";");
		onCreate(db);
		
	}

}
