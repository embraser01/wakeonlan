package com.embraser01.android.wakeonlan;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * class permettant d'afficher une boite de dialogue pour ajouter ou editer un PC
 */

public class NewPCDisplay extends DialogFragment {
	
	public static String TAG = "NewPCDisplay";

    public boolean duplicate_mode;
    public boolean edit_mode;
	public int id;
	
	public EditText Emac;
	public EditText Etitle;
	public EditText Eip;
	public EditText Eport;

	ListPC listBDD;
	NewPC newPC;
	
	public NewPCDisplay() {
	}
		
	public NewPCDisplay(boolean edit_mode, int id) {
		this.edit_mode = edit_mode;
		this.id = id;
	}

    public NewPCDisplay( int id, boolean duplicate_mode) {
        this.id = id;
        this.duplicate_mode = duplicate_mode;
    }

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
						Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.dialog_new, container, false);

		getDialog().setTitle(R.string.menu_add);
		getDialog().setCanceledOnTouchOutside(true);
		getDialog().setCancelable(true);
		
		Emac = (EditText) v.findViewById(R.id.mac);
		Etitle = (EditText) v.findViewById(R.id.title);
		Eip = (EditText) v.findViewById(R.id.ip);
		Eport = (EditText) v.findViewById(R.id.port);
		
		Button cancelBtn = (Button) v.findViewById(R.id.add_cancel);
		Button validateBtn = (Button) v.findViewById(R.id.add_validate);
		
		if (edit_mode == false && duplicate_mode == false) {
			Etitle.getText().clear();
			Emac.getText().clear();
			Eip.getText().clear();
			Eport.getText().clear();
		}
		else {
			listBDD = new ListPC(getActivity());
			listBDD.open();
			String edit_info [] = listBDD.selectPC(id);

            if (duplicate_mode == true)
			    Etitle.setText("Copy of " + edit_info[0]);
            else
                Etitle.setText(edit_info[0]);

			Emac.setText(edit_info[1]);
			Eip.setText(edit_info[2]);
			Eport.setText(edit_info[3]);
			
			listBDD.close();
		}
		
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		
		
		validateBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
								
				// enregistrement des donn�es rentr�es
				
				String mac = Emac.getText().toString();
				String title = Etitle.getText().toString();
				String ip = Eip.getText().toString();
				String port = Eport.getText().toString();
				
				if ((mac.equals("")) || (title.equals("")) || (ip.equals(""))) {
					String emptyEditBox = getResources().getString(R.string.emptyEditBox);
					Toast.makeText(getActivity(), emptyEditBox, Toast.LENGTH_LONG).show();
					return;
				}
				else {

					try {
						// validate mac address
						MagicPacket.validateMac(mac);

					}
					catch(IllegalArgumentException iae) {
						String error_msg = getResources().getString(R.string.mac_error);
						Toast.makeText(getActivity(),error_msg,Toast.LENGTH_SHORT).show();
						return;
					}
				}

                if (port.equals(""))
                    port = 9 + "";
				newPC = new NewPC( title, mac, ip, port);
				listBDD = new ListPC(getActivity());
				listBDD.open();
				
				if (edit_mode == false || duplicate_mode == true)
					listBDD.newPC(newPC);
				else
					listBDD.updatePC(id, newPC);
				
				listBDD.close();
				dismiss();
				((WakeOnLanActivity)getActivity()).updateList();
			}
		});
			
		return v;
	}
}
